#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/155a0000.ufs/by-name/RECOVERY$(getprop ro.boot.slot_suffix):42131472:f32d51b50bd9a52a496f018aed11b5ab262a2095; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/155a0000.ufs/by-name/BOOT$(getprop ro.boot.slot_suffix):36542480:19ab0a7e7cf98f9ed11dc0034e13a4b0bd0247a3 \
          --target EMMC:/dev/block/platform/155a0000.ufs/by-name/RECOVERY$(getprop ro.boot.slot_suffix):42131472:f32d51b50bd9a52a496f018aed11b5ab262a2095 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
