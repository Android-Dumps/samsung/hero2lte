#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Health
PRODUCT_PACKAGES += \
    android.hardware.health@2.1-impl \
    android.hardware.health@2.1-impl.recovery \
    android.hardware.health@2.1-service

# Overlays
PRODUCT_ENFORCE_RRO_TARGETS := *

# Product characteristics
PRODUCT_CHARACTERISTICS := default

# Rootdir
PRODUCT_PACKAGES += \
    install-recovery.sh \

PRODUCT_PACKAGES += \
    fstab.samsungexynos8890 \
    init.baseband.rc \
    init.samsung.rc \
    init.samsungexynos8890.rc \
    init.samsungexynos8890.usb.rc \
    init.recovery.samsungexynos8890.rc \

# Shipping API level
PRODUCT_SHIPPING_API_LEVEL := 23

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

# Inherit the proprietary files
$(call inherit-product, vendor/samsung/hero2lte/hero2lte-vendor.mk)
