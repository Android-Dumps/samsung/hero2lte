#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_hero2lte.mk

COMMON_LUNCH_CHOICES := \
    lineage_hero2lte-user \
    lineage_hero2lte-userdebug \
    lineage_hero2lte-eng
